#!/bin/sh

#  Base.sh
#  
#
#  Created by David Kittell on 6/1/17.
#

clear

echo -e "\033[32mRed Hat - Base Setup - Start\033[0m"

echo -e "\033[32mChecking UNIX Identification\033[0m"
if [ -f /opt/UnixIdentification.sh ]; then
sudo rm -f /opt/UnixIdentification.sh
sudo wget -P /opt/ https://gitlab.com/Kittell-Projects/Unix/unixidentification/raw/master/UnixIdentification.sh
else
sudo wget -P /opt/ https://gitlab.com/Kittell-Projects/Unix/unixidentification/raw/master/UnixIdentification.sh
fi

sh /opt/UnixIdentification.sh -d
case "$DISTRO_ID" in
    ${DISTRO_ID_RHEL_CLIENT}|${DISTRO_ID_RHEL_SERVER}|${DISTRO_ID_RHEL_WORKSTATION})
    vRHEL=$(sh /opt/UnixIdentification.sh -r)
#echo $vRHEL

case "$vRHEL" in
    '7.0'|'8.0')
      ;;
    
    *)
      echo "$DISTRO_ID\n$DISTRO_DESC not supported"
      exit
      ;;
  esac
      ;;
    
    *)
      echo "$DISTRO_ID\n$DISTRO_DESC not supported"
      exit
      ;;
  esac




echo -e "\033[32mChecking Red Hat Registration\033[0m"
if [ "$(sudo subscription-manager status| grep "Overall Status")" != "Overall Status: Unknown" ]; then
    clear
    echo "Properly Registered"
else
    clear

    echo -e "\033[32mRed Hat Registration - Start\033[0m"
    if [ -z ${1+x} ]; then
        echo -e "\033[01m\e[4mType your username for RedHat.com, followed by [ENTER]:\e[0m\033[0m"
        read rhUser
    else
        declare rhUser=$1
    fi

    if [ -z ${2+x} ]; then
        echo -e "\033[01m\e[4mType your password for RedHat.com, followed by [ENTER]:\e[0m\033[0m"
        read -s rhPass
    else
        declare rhPass=$2
    fi

    clear
    echo -e "\033[32mSet Server Hostname - Start\033[0m"
    if [ -z ${3+x} ]; then
        echo -e "\033[01m\e[4mType your desired hostname for the server, followed by [ENTER]:\e[0m\033[0m"
        read hostname
        sudo hostnamectl set-hostname $hostname
    else
        declare hostname=$3
        sudo hostnamectl set-hostname $hostname
    fi

    echo -e "\033[32mSet Server Hostname - Stop\033[0m"
    # Register Red Hat Server - Start
    sudo subscription-manager register --username $rhUser --password $rhPass --auto-attach
    clear
    sudo subscription-manager refresh
    clear
    history -c
    sudo subscription-manager identity
    # Register Red Hat Server - Stop
    echo -e "\033[32mRed Hat Registration - Stop\033[0m"
fi

# Setup Node non-specific - Start
sudo subscription-manager list # Typically not needed
sudo yum repolist all # Typically not needed

if [ $(echo $vRHEL | cut -d '.' -f1) = '7' ]; then 
echo 'Red Hat 7'
echo -e "\033[32mEnabling Additional Official Repos\033[0m"
sudo subscription-manager repos --enable=rhel-7-server-optional-rpms
sudo subscription-manager repos --enable=rhel-7-server-supplementary-rpms
sudo subscription-manager repos --enable=rhel-7-server-thirdparty-oracle-java-rpms
sudo subscription-manager repos --enable=rhel-7-server-dotnet-rpms
elif  [ $(echo $vRHEL | cut -d '.' -f1) = '8' ]; then 
echo 'Red Hat 8'
fi

echo -e "\033[32mCreating Yum Cache\033[0m"
sudo yum -y makecache fast

echo -e "\033[32mRunning Yum Update\033[0m"
sudo yum -y update # Make sure you are up to date before you start

echo -e "\nCurrent timezone is $(timedatectl | grep "Time zone" | cut -d ":" -f2 | sed 's/^[ \t]*//;s/[ \t]*$//')"
echo -e "\033[01m\e[4mDo you need to set/change the timezone, if yes type y or if no type n followed by [ENTER]:\e[0m\033[0m"
read tzSet

case $tzSet in
    [yY])
        echo -e "\nTimezone should be in format similar to 'America/Detroit'"
        echo -e "\033[01m\e[4mType your desired timezone for the server, followed by [ENTER]:\e[0m\033[0m"
        read tzNew
        echo -e "\033[32mSetting Timezone\033[0m"
        sudo timedatectl set-timezone $tzNew # Optional but make sure you have the correct timezone
    ;;
    *)
        echo -e "\nTimezone is $(timedatectl | grep "Time zone" | cut -d ":" -f2 | sed 's/^[ \t]*//;s/[ \t]*$//')"
    ;;
esac

# Install needed applications - start
echo -e "\033[32mInstalling Needed Applications\033[0m"
sudo yum -y install net-tools bind-utils nano wget tcl unzip bzip2 ed gcc make libstdc++.so.6 telnet
# Install needed applications - stop
# Setup Node non-specific - Stop

# Setup SSH Banner
echo -e "\033[01m\e[4mDo you want to set the SSH banner, if yes type y or if no type n followed by [ENTER]:\e[0m\033[0m"
read bannerSet

case $bannerSet in
[yY])
sh -c "$(curl -s "https://gitlab.com/Kittell-Projects/RedHat/BaseTemplate/raw/master/setup-banner.sh")"
;;
*)

;;
esac

