#!/bin/sh

#  setup-banner.sh
#  
#
#  Created by David Kittell on 6/1/17.
#

echo -e "\033[01m\e[4mType your company name, followed by [ENTER]:\e[0m\033[0m"
read companyname

companyPolicy="$(echo $companyname)\n\r\n\rWARNING\n\rThis computer system is the property of $(echo $companyname). It may be accessed and used only for authorized $(echo $companyname) business by authorized personnel. Unauthorized access or use of this computer system may subject violators to criminal, civil and/or administrative disciplinary action.\n\r\n\r$(echo $companyname) may monitor or log any activity or communication on the system and retrieve any information stored within the system.  By accessing and using this computer, you are consenting to such monitoring and information retrieval for law enforcement and other purposes. All information accessed via this system should be considered confidential unless otherwise indicated. Access or use of this computer system by any person, whether authorized or unauthorized, constitutes consent to these terms. There is no right of privacy in this system.\n\r\n\rNOTE: By logging into this system you indicate your awareness of and consent to these terms and conditions of use. LOG OFF IMMEDIATELY if you do not agree to the conditions stated in this warning."

clear
echo -e "$companyPolicy" | sudo tee -a /etc/banner
cat /etc/banner

OS=$(cat /etc/redhat-release)
echo $OS

banner=$(echo "\n$OS\n")
banner=${banner}$(echo "$(companyname)\n")
banner=${banner}$(echo "Hostname: $(hostname)\n")
banner=${banner}$(echo "Network Information\n")

NetworkPorts=$(ip link show | grep '^[a-z0-9]' | awk -F : '{print $2}')
for val in $(echo $NetworkPorts); do   # Get for all available hardware ports their status
#echo "Current Interface: $val"
netActive=$(ifconfig $val | grep "inet.*broadcast" -B1 | grep "flags" | cut -d ":" -f1)
echo $netActive

if [ "$netActive" == "$val" ]
then
netIP=$(/sbin/ip -o -4 addr list $val | awk '{print $4}' | cut -d/ -f1)
netMask=$(ipcalc -m $netIP | cut -d '=' -f2)
netCIDR=$(ipcalc -p $netIP $netMask | cut -d '=' -f2)
netWork=$(ipcalc -n $netIP $netMask | cut -d '=' -f2)

banner=${banner}$(echo "    Adapter:                 $val\n")
banner=${banner}$(echo "    IP:                      $netIP\n")
banner=${banner}$(echo "    Netmask:                 $netMask\n")
banner=${banner}$(echo "    CIDR:                    $netWork/$netCIDR\n")

fi

done

echo -e $banner

clear
echo -e "$banner"|sudo tee /etc/motd

cat /etc/motd

# Set SSH Banner
sudo cp /etc/ssh/sshd_config /etc/ssh/sshd_config.original
sudo sed -i "s|#Banner none|Banner /etc/banner|" /etc/ssh/sshd_config
sudo service sshd restart
