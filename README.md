# Red Hat Templates

This project is a set of scripts to initially setup a new Red Hat server

* * *

**Server Templates:**

Base Template
<pre>sudo sh -c "$(curl -s "https://gitlab.com/Kittell-Projects/Unix/RedHat/BaseTemplate/raw/master/Base.sh")"</pre>

SSH Banner
<pre>sudo sh -c "$(curl -s "https://gitlab.com/Kittell-Projects/Unix/RedHat/BaseTemplate/raw/master/setup-banner.sh")"</pre>
